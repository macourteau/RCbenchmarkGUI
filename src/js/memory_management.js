// Call this first, with default value and ID
// Use the NEW keyword
function storedVariable(value, chromeID) {
    this.value = value;
    this.chromeID = chromeID;
    this.isStoredVariable = true;
}

// Must be called with the previously created "Stored variable" object, before your app begins. Will load default OR stored variable if available
function initialMemoryLoad(callback, storageObj) {
    chrome.storage.local.get(storageObj.chromeID, function afterChromeMemory(result) {
        if (result[storageObj.chromeID] !== undefined) {
            storageObj.value = result[storageObj.chromeID];
        }

        callback(storageObj.value);
    });
}

// Call this, with the storage object created to update its value. Will automatically save to chrome storage at the same time.
// To read the value, you must write by yourself storageObject.value
// DO NOT WRITE to storageObject.value, use this function instead
function changeMemory(storageObj, value) {
    storageObj.value = value;
    var toStore = {};
    toStore[storageObj.chromeID] = value;
    chrome.storage.local.set(toStore);
}