'use strict';

function refreshConnectButton(){
  $("#connect-progress").hide();
  $("#btn-connecting").hide();
  $("#btn-connect").show();
  $("#btn-disconnect").hide();
  $("div#connectionInfo").hide();
  
  if(isConnected()){
    $("#btn-disconnect").show();
    $("#btn-connect").hide();
  }else{
    if(isConnecting()){
      $("#connect-progress").show();
      $("#btn-connecting").show();
      $("#btn-connect").hide();
    }else{
      if(GUI.wrong_port){
        $("div#connectionInfo").html(getMessage('wrongPort'));
      }else{
        $("div#connectionInfo").html(getMessage('notConnected'));
      }
      $("div#connectionInfo").show();
    }  
  }
}

$(document).ready(function () {
	$('div#port-picker a.connect').click(function () {
		if(!GUI.firmware_is_flashing) {
          connection();
        };
	});
	
    PortHandler.initialize();
    PortUsage.initialize();
    refreshConnectButton();
});

function isConnected(){
  return GUI.connected_to && (GUI.connected_to.valueOf().trim() != "");
}

function isConnecting(){
  return GUI.connecting_to;
}

function disconnect() {
  GUI.timeout_kill_all();
  GUI.interval_kill_all();
  GUI.tab_switch_cleanup();
  GUI.tab_switch_in_progress = false;

  serial.disconnect(onClosed);

  var wasConnected = CONFIGURATOR.connectionValid;

  GUI.connected_to = false;
  CONFIGURATOR.connectionValid = false;
  GUI.allowedTabs = GUI.defaultAllowedTabsWhenDisconnected.slice();
  MSP.disconnect_cleanup();
  PortUsage.reset();

  // Reset various UI elements
  $('span.i2c-error').text(0);
  $('span.cycle-time').text(0);

  //hide left pane elements
  $('#left-pane #info p#warning, div#data, #left-pane #info p.calibrationThrust, #left-pane #info p.calibrationTorque, #left-pane #info p.oldCalibrationThrust, #left-pane #info p.oldCalibrationTorque').hide();

  // unlock port select & baud
  $('div#port-picker #port').prop('disabled', false);
  if (!GUI.auto_connect) $('div#port-picker #baud').prop('disabled', false);

  // reset connect / disconnect button
  $("div#hardware").hide();
  $('div#port-picker a.connect').removeClass('active');
  $('#polesSetting').hide();

  // reset active sensor indicators
  sensor_status(0);

  if (wasConnected) {
      if(!GUI.firmware_is_flashing) $('#tabs .tab_landing a').click();
  }
}

function connection() {
	if (GUI.connect_lock != true) { // GUI control overrides the user control
		var clicks = $(this).data('clicks'),
			selected_port = String($('div#port-picker #port').val()),
			selected_baud = parseInt($('div#port-picker #baud').val());

		if (selected_port != '0' && selected_port != 'DFU') {
			if (!isConnected()) {
				console.log('Connecting to: ' + selected_port);
				GUI.connecting_to = selected_port;

				// lock port select & baud while we are connecting / connected
                $("div#hardware").hide();
				$('div#port-picker #port, div#port-picker #baud, div#port-picker #delay').prop('disabled', true);
				MSP.ready_detected = false;
				serial.connect(selected_port, {bitrate: selected_baud}, onOpen);
			} else {
                disconnect();
			}

			$(this).data("clicks", !clicks);
		}else{
          console.log("Invalid port: " + selected_port);
        }
	}else{
      console.log("GUI connection lock"); 
    }
    refreshConnectButton();
};

function onOpen(openInfo) {
    var wasFlashing = GUI.firmware_is_flashing;
	GUI.firmware_is_flashing = false;
    if (openInfo) {
		GUI.log(getMessage('serialPortOpened', [openInfo.connectionId]));

        //setTimeout(function(){
        //	if(!MSP.ready_detected) CONFIG.readyCallback(); //In case the board reset pulse didn't work
        //}, 3000);
      
        function showFirmwareFlashing(){
          //$('a.connect').click(); //disconnect
          GUI.connecting_to = false;
          refreshConnectButton();
          
          GUI.timeout_kill_all();
          serial.disconnect();
          $('#tabs .tab_setup a').click();
          setFirmwareLabels();
          $('#polesSetting').hide();
        }
      
        // disconnect with error if we don't get IDENT data
        GUI.timeout_add('connecting', function () {
            if (!CONFIGURATOR.connectionValid) {
                GUI.log(getMessage('noConfigurationReceived'));
                CONFIG.firmwareVersion = 'tryAgain';
                showFirmwareFlashing();
                $("div#connectionInfo").html(getMessage('notConnectedTimeout'));
            }
        }, 15000);

        serial.onReceive.addListener(read_serial);

        //Waits for the board to send its "Ready" string.
        CONFIG.readyCallback = function() {
        	MSP.ready_detected = true;
            CONFIG.controlBoard_flag_active = false;
            
            // request configuration data
            MSP.send_message(MSP_codes.MSP_FIRMWARE, false, false, function () {
                GUI.log(getMessage('firmwareVersion', [CONFIG.firmwareVersion]));
                googleAnalytics.sendEvent('Firmware', 'Using', CONFIG.firmwareVersion);

                if (CONFIG.firmwareVersion === CONFIGURATOR.firmwareVersionAccepted15xx || CONFIG.firmwareVersion === CONFIGURATOR.firmwareVersionAccepted1780) {
                    MSP.send_message(MSP_codes.MSP_BOARD, false, false, function () {
                        // after board version received, if firmware is incorrect automaticallly disconnect
                        if(CONFIG.boardVersion === "Series 1780" && CONFIG.firmwareVersion !== CONFIGURATOR.firmwareVersionAccepted1780){
                          GUI.log(getMessage('firmwareVersionWrongBoard', [CONFIG.firmwareVersion, CONFIGURATOR.firmwareVersionAccepted1780]));
                          CONFIG.firmwareVersion = 'wrongHardware';
                          showFirmwareFlashing();
                          return;
                        }
                        if(CONFIG.boardVersion !== "Series 1780" && CONFIG.firmwareVersion !== CONFIGURATOR.firmwareVersionAccepted15xx){
                          GUI.log(getMessage('firmwareVersionWrongBoard', [CONFIG.firmwareVersion, CONFIGURATOR.firmwareVersionAccepted15xx]));
                          CONFIG.firmwareVersion = 'wrongHardware';
                          showFirmwareFlashing();
                          return;
                        }
                      
                        var boardLog = getMessage('boardInfoReceived', [CONFIG.displayVersion]);
                        boardLog += getMessage('boardInfoReceivedId', [CONFIG.boardId]);
                        GUI.log(boardLog);
                        googleAnalytics.sendEvent('Board', 'Using', CONFIG.displayVersion);
                       
                        var plotsWrappers = '<label><b>Hardware:</b></br>Connected!</br>';
                        plotsWrappers += CONFIG.displayVersion + ', ';
                        plotsWrappers += 'firmware v' + CONFIG.firmwareVersion;
                        plotsWrappers += '</br><span class="usb">Update Rate: <value class="usb"></value> Hz</span>\n';
                        plotsWrappers += '<\label></br></br>';
                        $('#data-pane #hardware').html(plotsWrappers); 
                        $("div#hardware").show();
                        $('#polesSetting').show();


                        // Activate allowed tabs and setup GUI as connected
                        CONFIGURATOR.connectionValid = true;
                        CONFIGURATOR.firstReadingDiscarted = false;
                        GUI.allowedTabs = GUI.defaultAllowedTabsWhenConnected.slice();
                      
                        // update connected_to
                        GUI.connected_to = GUI.connecting_to;

                        // reset connecting_to
                        GUI.connecting_to = false;
                      
                      
                        // save selected port with chrome.storage if the port differs
                        chrome.storage.local.get('last_used_port', function (result) {
                            if (result.last_used_port) {
                                if (result.last_used_port != GUI.connected_to) {
                                    // last used port doesn't match the one found in local db, we will store the new one
                                    chrome.storage.local.set({'last_used_port': GUI.connected_to});
                                }
                            } else {
                                // variable isn't stored yet, saving
                                chrome.storage.local.set({'last_used_port': GUI.connected_to});
                            }
                        });
                      
                        onConnect();

                        // For safety disable all output and put motors to their cutoffs
                        OUTPUT_DATA.active[0] = 0;
                        OUTPUT_DATA.active[1] = 0;
                        OUTPUT_DATA.ESC_PWM = CONFIG.ESCCutoff.value;
                        OUTPUT_DATA.active[2] = 0;
                        OUTPUT_DATA.active[3] = 0;
                        OUTPUT_DATA.ESCA = CONFIG.ESCCutoffA.value;
                        OUTPUT_DATA.ESCB = CONFIG.ESCCutoffB.value;
                        
                        // Control board
                        TABS.motors.controlBoardInit();

                        handleCalibration();
                      
                        //Firmware labels
                        setFirmwareLabels();
                      
                        // Since the feature is hidden for the 1780, make sure the optical RPM is the main one                        
                        if(CONFIG.boardVersion === "Series 1780"){
                          setMainRPMsensor("optical");
                        }

                        // Select what tab is shown when connected
                        TABS.sensors.initialize(); //starts the plots scrolling
                        repeated_sensor_poll(); // Automatically start sensors polling 

                        //turns on the plots
                        clearInterval(USER_LIMITS.refreshRate);
                        USER_LIMITS.refreshRate = setInterval(update_plots, CONFIG.plotsRateHz.value);
					});
                } else {
                    GUI.log(getMessage('firmwareVersionNotSupported', [CONFIG.firmwareVersion]));
                    showFirmwareFlashing();
                }
            });
        }
        GUI.wrong_port = false;
    } else {
        console.log('Failed to open serial port');
        GUI.log(getMessage('serialPortOpenFail'));
		
		$('#polesSetting').hide();
        //$('div#port-picker a.connect').text(getMessage('connect'));
        $('div#port-picker a.connect').removeClass('active');
				
        // unlock port select & baud
        $('div#port-picker #port, div#port-picker #baud, div#port-picker #delay').prop('disabled', false);

        // reset data
        $('div#port-picker a.connect').data("clicks", false);
        GUI.connecting_to = false;
        GUI.wrong_port = true;
    }
    refreshConnectButton();
}

function display_text(){
	
	//Generate text to display (with limited update rate)
  	var currTimeMs = window.performance.now(); //time in ms
	var dt = (currTimeMs - display_text.lastTimestamp);
	if(dt>150 || isNaN(dt)){
		 if (CONFIG.firmwareVersion === CONFIGURATOR.firmwareVersionAccepted15xx || CONFIG.firmwareVersion === CONFIGURATOR.firmwareVersionAccepted1780) {
                  
		 var plotsWrappers = '<label><b>'+getMessage("hardware")+'</b></br>'+getMessage("hardwareConnected")+'</br>';
                        plotsWrappers += CONFIG.boardVersion + ', ';
                        plotsWrappers += getMessage("firmwareV") + CONFIG.firmwareVersion;
                        plotsWrappers += '</br><span class="usb">'+getMessage("updateRate")+'<value class="usb"></value> Hz</span>\n';
                        plotsWrappers += '<\label></br></br>';
                        $('#data-pane #hardware').html(plotsWrappers);
          }						
		 plotsWrappers = '<dl>\n';
        var temperatures = DATA.getTemperatures();
		if(CONFIGURATOR.connectionValid){
            plotsWrappers += '<dd><b>';
            var sideID = "";
            if(CONFIG.s1780detected.Bside){
              plotsWrappers += getMessage('sensorsSideAForSerial_Backend');
              sideID = "A";
            }else{
              plotsWrappers += getMessage('sensorsForSerial_Backend');
            }
			plotsWrappers += '</b></dd>\n';                
			plotsWrappers += '<dt class="voltage' + sideID + '">'+getMessage('voltageForSerial_Backend')+'<value class="voltage"></value> V</dt>\n';
			plotsWrappers += '<dt class="current' + sideID + '">'+getMessage('currentForSerial_Backend')+'<value class="current"></value> A</dt>\n';
            plotsWrappers += '<dt class="power' + sideID + '">'+getMessage('elecPowerForSerial_Backend')+'<value class="power"></value> W</dt>\n';
			plotsWrappers += '<dt class="thrust' + sideID + '">'+getMessage('thurstForSerial_Backend')+'<value class="thrust"></value> ' + UNITS.text[UNITS.display['thrust']] + '</dt>\n';
			if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1780") plotsWrappers += '<dt class="torque' + sideID + '">'+getMessage('torqueForSerial_Backend')+'<value class="torque"></value> ' + UNITS.text[UNITS.display['torque']] + '</dt>\n';
			if(CONFIG.boardVersion === "Series 1580") plotsWrappers += '<dt class="weight">'+getMessage('weightForSerial_Backend')+'<value class="weight"></value> ' + UNITS.text[UNITS.display['weight']] + '</dt>\n';
            if(CONFIG.boardVersion === "Series 1580") plotsWrappers += '<dt class="vibration">'+getMessage('vibrationForSerial_Backend')+'<value class="vibration"></value> g</dt>\n';
            if(CONFIG.boardVersion === "Series 1520" || CONFIG.boardVersion === "Series 1580"){
              if(CONFIG.electricalRPMActive.value){
                  plotsWrappers += '<dt class="rpmElectrical">'+getMessage('motorSpeedForSerial_Backend') + CONFIG.numberOfMotorPoles.value + getMessage('polesForSerial_Backend')+'<value class="rpm"></value> ' + UNITS.text[UNITS.display['motorSpeed']] + '</dt>\n';
              }
            }
            if(CONFIG.opticalRPMActive.value){
                var plural = "";
                if(CONFIG.numberOfOpticalTape.value>1) 
                    plural = "s";
                plotsWrappers += '<dt class="rpmOptical' + sideID + '">'+getMessage('motorSpeedForSerial_Backend') + CONFIG.numberOfOpticalTape.value + getMessage('tapeForSerial_Backend')+ plural + ': <value class="optical_rpm"></value> ' + UNITS.text[UNITS.display['motorSpeed']] + '</dt>\n';
            }
            if(CONFIG.s1780detected.Bside){
              plotsWrappers += '<dd><b><br/>';
              plotsWrappers += getMessage('sensorsSideBForSerial_Backend');
              plotsWrappers += '</b></dd>\n';
              plotsWrappers += '<dt class="voltageB">'+getMessage('voltageForSerial_Backend')+'<value class="voltageB"></value> V</dt>\n';
              plotsWrappers += '<dt class="currentB">'+getMessage('currentForSerial_Backend')+'<value class="currentB"></value> A</dt>\n';
              plotsWrappers += '<dt class="powerB">'+getMessage('elecPowerForSerial_Backend')+'<value class="powerB"></value> W</dt>\n';
              plotsWrappers += '<dt class="thrustB">'+getMessage('thurstForSerial_Backend')+'<value class="thrustB"></value> ' + UNITS.text[UNITS.display['thrust']] + '</dt>\n';
              plotsWrappers += '<dt class="torqueB">'+getMessage('torqueForSerial_Backend')+'<value class="torqueB"></value> ' + UNITS.text[UNITS.display['torque']] + '</dt>\n';
              var plural = "";
              if(CONFIG.numberOfOpticalTapeB.value>1) 
                  plural = "s";
              plotsWrappers += '<dt class="rpmOpticalB">'+getMessage('motorSpeedForSerial_Backend') + CONFIG.numberOfOpticalTapeB.value + getMessage('tapeForSerial_Backend') + plural + ': <value class="optical_rpmB"></value> ' + UNITS.text[UNITS.display['motorSpeed']] + '</dt>\n';
              
              plotsWrappers += '<dd><b><br/>';
              plotsWrappers += getMessage('sensorsCombined');
              plotsWrappers += '</b></dd>\n';
              plotsWrappers += '<dt class="powerDual">'+getMessage('elecPowerForSerial_Backend')+'<value class="powerDual"></value> W</dt>\n';
              plotsWrappers += '<dt class="thrustDual">'+getMessage('thurstForSerial_Backend')+'<value class="thrustDual"></value> ' + UNITS.text[UNITS.display['thrust']] + '</dt>\n';
              plotsWrappers += '<dt class="torqueDual">'+getMessage('torqueForSerial_Backend')+'<value class="torqueDual"></value> ' + UNITS.text[UNITS.display['torque']] + '</dt>\n';
            }
            for(var i=0; i<temperatures.length; i++){
                  var temp = temperatures[i];
                  var id = "temp" + temp.id;
                  var label = getMessage("temp") + " " + temp.label + ": ";
                  var unit = UNITS.text[UNITS.display['temperature']];
                  plotsWrappers += '<dt class="' + id + '">' + label + '<value class="' + id + '"></value> ' + unit + '</dt>\n';
              };
            if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1780"){
                if (DATA.isPressureSensorAvailable()) {
                    plotsWrappers += '<dt class="airspeed">'+getMessage('airSpeedForSerial_Backend')+'<value class="airspeed"></value> ' + UNITS.text[UNITS.display['speed']] + '</dt>\n';
                }
            }
			if(CONFIG.developper_mode) {
                plotsWrappers += '<dl>\n';
                plotsWrappers += '<dd><br/><b>'+getMessage('deBugForSerial_Backend')+'</b></dd>\n';
				if(CONFIG.boardVersion === "Series 1580")plotsWrappers += '<dt class="loadleft">'+getMessage('loadleftForSerial_Backend')+'<value class="loadleft"></value> mV</dt>\n';
				if(CONFIG.boardVersion === "Series 1580")plotsWrappers += '<dt class="loadright">'+getMessage('loadrightForSerial_Backend')+'<value class="loadright"></value> mV</dt>\n';
				if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520")plotsWrappers += '<dt class="loadthrust">'+getMessage('loadthrustForSerial_Backend')+'<value class="loadthrust"></value> mV</dt>\n';
                if(CONFIG.boardVersion === "Series 1780"){
                  /*for (var i=0;i<6;i++){
                    plotsWrappers += '<dt class="load' + i + '">Load Cell ' + i + ': <value class="load' + i + '"></value> tared</dt>\n'
                  }*/
                  if(CONFIG.s1780detected.LCA){
                    for (var i=0;i<6;i++){
                      plotsWrappers += '<dt class="loadNTA' + i + '">' + getMessage("Load Cell") + " ";
                      if(CONFIG.s1780detected.LCB){
                        plotsWrappers += 'A';
                      }
                      plotsWrappers += i + ': <value class="loadNTA' + i + '"></value> raw</dt>\n';
                    } 
                  }
                  if(CONFIG.s1780detected.LCB){
                    for (var i=0;i<6;i++){
                      plotsWrappers += '<dt class="loadNTB' + i + '">' + getMessage("Load Cell") + " ";
                      plotsWrappers += 'B';
                      plotsWrappers += i + ': <value class="loadNTB' + i + '"></value> raw</dt>\n';
                    } 
                  }
                }
                plotsWrappers += '<dt class="currentBurst">'+getMessage('currentBurstForSerial_Backend')+'<value class="currentBurst"></value> A</dt>\n';
                if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1580"){
                  plotsWrappers += '<dt class="pinPwm">'+getMessage('pinStateESCForSerial_Backend')+'<value class="pinPwm"></value></dt>\n';
                  plotsWrappers += '<dt class="pinServo1">'+getMessage('pinStateServo1ForSerial_Backend')+'<value class="pinServo1"></value></dt>\n';
                  plotsWrappers += '<dt class="pinServo2">'+getMessage('pinStateServo2ForSerial_Backend')+'<value class="pinServo2"></value></dt>\n';
                  plotsWrappers += '<dt class="pinServo3">'+getMessage('pinStateServo3ForSerial_Backend')+'<value class="pinServo3"></value></dt>\n';
                }
                if(CONFIG.boardVersion === "Series 1780"){
                  plotsWrappers += '<dt class="pinOptA">'+getMessage('opticalSensorAPin')+'<value class="pinOptA"></value></dt>\n';
                  plotsWrappers += '<dt class="pinOptB">'+getMessage('opticalSensorBPin')+'<value class="pinOptB"></value></dt>\n';
                }
                plotsWrappers += '<dt class="debug0">'+getMessage('deBug0')+'<value class="debug0"></value></dt>\n';
				plotsWrappers += '<dt class="debug1">'+getMessage('deBug1')+'<value class="debug1"></value></dt>\n';
				plotsWrappers += '<dt class="debug2">'+getMessage('deBug2')+'<value class="debug2"></value></dt>\n';
				plotsWrappers += '<dt class="debug3">'+getMessage('deBug3')+'<value class="debug3"></value></dt>\n';
			}
			plotsWrappers += '<dt class="none"><br></dt>\n';
			plotsWrappers += '<dt class="cutoff" style="display:none">'+getMessage('safetyCutoffActivated')+'</dt>\n';
		}else{
			plotsWrappers += '<dd><b></b></dd>\n';
		}
		plotsWrappers += '<\dl>';
		$('#data-pane #data').html(plotsWrappers);

		$('value.voltage').html(DATA.getESCVoltage().toFixed(2));
		$('value.current').html(DATA.getESCCurrent().toFixed(2));
        $('value.power').html(DATA.getElectricalPower().toFixed(0));
		$('value.thrust').html(UNITS.convertToDisplay('thrust', DATA.getThrust()).toFixed(3));
      
        if(CONFIG.s1780detected.Bside){
          $('value.voltageB').html(DATA.getESCVoltage(true).toFixed(2));
          $('value.currentB').html(DATA.getESCCurrent(true).toFixed(2));
          $('value.powerB').html(DATA.getElectricalPower(true).toFixed(0));
          $('value.thrustB').html(UNITS.convertToDisplay('thrust', DATA.getThrust(true)).toFixed(3)); 
          $('value.torqueB').html(UNITS.convertToDisplay('torque', DATA.getTorque(true)).toFixed(3));
          $('value.optical_rpmB').html(Math.round(UNITS.convertToDisplay('motorSpeed', DATA.getOpticalRPM(true)).toFixed(3)));
          
          $('value.powerDual').html(DATA.getDualElectricalPower().toFixed(0));
          $('value.thrustDual').html(UNITS.convertToDisplay('thrust', DATA.getDualThrust()).toFixed(3)); 
          $('value.torqueDual').html(UNITS.convertToDisplay('torque', DATA.getDualTorque()).toFixed(3));
        }
      
		if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1780")$('value.torque').html(UNITS.convertToDisplay('torque', DATA.getTorque()).toFixed(3));
		if(CONFIG.boardVersion === "Series 1580")$('value.weight').html(UNITS.convertToDisplay('weight', DATA.getWeight()).toFixed(3));
        if(CONFIG.boardVersion === "Series 1580")
          $('value.vibration').html(SENSOR_DATA.vibration.toFixed(1));
        if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1580"){
          $('value.rpm').html(Math.round(UNITS.convertToDisplay('motorSpeed', DATA.getElectricalRPM()).toFixed(3)));
        }
        $('value.airspeed').html(UNITS.convertToDisplay('speed', DATA.getAirSpeed()).toFixed(2));
        $('value.optical_rpm').html(Math.round(UNITS.convertToDisplay('motorSpeed', DATA.getOpticalRPM()).toFixed(3)));
        for(var i=0; i<temperatures.length; i++){    
            var temp = temperatures[i];
            var id = "temp" + temp.id;
            $('value.' + id).html(UNITS.convertToDisplay('temperature', temp.value).toFixed(1));
        };
        if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1780"){
          $('value.airpressure').html(UNITS.convertToDisplay('pressure', DATA.getPressure()).toFixed(2) +
                                   " " + UNITS.text[UNITS.display['pressure']]);
        }
        $('value.usb').html(SENSOR_DATA.updateRate.toFixed(0));
		if(CONFIG.developper_mode){	
			if(CONFIG.boardVersion === "Series 1580")$('value.loadleft').html(DATA.getLoadCellLeft().toFixed(4));
			if(CONFIG.boardVersion === "Series 1580")$('value.loadright').html(DATA.getLoadCellRight().toFixed(4));
			if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520")$('value.loadthrust').html(DATA.getLoadCellThrust().toFixed(4));
            if(CONFIG.boardVersion === "Series 1780"){
              /*for (var i=0;i<6;i++){
                $('value.load' + i).html(DATA.getSixAxisTared(i).toFixed(4));
              }*/
              
              for (var i=0;i<6;i++){
                $('value.loadNTA' + i).html(SENSOR_DATA.sixAxisForcesRawA[i].toFixed(4));
                $('value.loadNTB' + i).html(SENSOR_DATA.sixAxisForcesRawB[i].toFixed(4));
              }
              
            }
            $('value.currentBurst').html(DATA.getESCCurrentBurst().toFixed(2));
            if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1580"){
              $('value.pinPwm').html(SENSOR_DATA.pinState[0]);
              $('value.pinServo1').html(SENSOR_DATA.pinState[1]);
              $('value.pinServo2').html(SENSOR_DATA.pinState[2]);
              $('value.pinServo3').html(SENSOR_DATA.pinState[3]);
            }
            if(CONFIG.boardVersion === "Series 1780"){
              $('value.pinOptA').html(SENSOR_DATA.pinState[0]);
              $('value.pinOptB').html(SENSOR_DATA.pinState[1]);
            }
            $('value.debug0').html(SENSOR_DATA.debug[0]);
			$('value.debug1').html(SENSOR_DATA.debug[1]);
			$('value.debug2').html(SENSOR_DATA.debug[2]);
			$('value.debug3').html(SENSOR_DATA.debug[3]);
		}
		display_text.lastTimestamp = currTimeMs;
      
		
	}
}

function commError(){
  var err = getMessage('commError');
  alert(err);
  chrome.runtime.reload();
}

function repeated_sensor_poll() {
	//Calculate the USB rate
  	var currTimeMs = window.performance.now(); //time in ms
	var dt = 0.001 * (currTimeMs - repeated_sensor_poll.lastTimestamp);
	SENSOR_DATA.updateRate = 0.95*(SENSOR_DATA.updateRate || 0) + 0.05/dt; //frequency in Hz
  
    //If above 300Hz, it is a communication bug
    if(SENSOR_DATA.updateRate>300){
      commError();
    }
  
  	repeated_sensor_poll.lastTimestamp = currTimeMs;

	display_text();
	safetyLimits();	
	
	script_update();
	logSample.update();
    
    selectPlotDefaultVal();

	MSP.send_poll(repeated_sensor_poll);
} 

function updateLeftPanelView(show){
    if(show) $('div#data').show();
    $("#left-pane-tare-button a").on('click', function() {
        var sel = $(this);
        sel.text(getMessage('pleaseWait'));
        googleAnalytics.sendEvent('Tare', 'Click');
        DATA.tareLoadCells(function(){
            if(CONFIG.boardVersion === "Series 1520"){
              sel.text(getMessage('tareThrust')); 
            }else{
              sel.text(getMessage('tareLoadCells'));
            }
        });
    });
    $("#left-pane-tare-airspeed-button a").on('click', function() {
        var sel = $(this);
        sel.text(getMessage('pleaseWait'));
        googleAnalytics.sendEvent('TareAirspeed', 'Click');
        DATA.tarePressure(function(){
            sel.text(getMessage('tareAirSpeed'));   
        });
    });
    $("#left-pane-tare-current-button a").on('click', function() {
        var sel = $(this);
        sel.text(getMessage('pleaseWait'));
        googleAnalytics.sendEvent('TareCurrent', 'Click');
        DATA.tareCurrent(function(){
            sel.text(getMessage('tareCurrent'));   
        });
    });
    if(show) {
        $("#left-pane-tare-button a").show();
        if(CONFIG.boardVersion === "Series 1780"){
          $("#left-pane-tare-current-button a").show();   
        }
        setTimeout( function () {
                if( DATA.isPressureSensorAvailable()) {
                    $("#left-pane-tare-airspeed-button a").show();
                }
            }, 300);
    }

}

function onConnect() {
    GUI.timeout_remove('connecting'); // kill connecting timer
	$('#tabs ul.mode-connected').removeClass('tab-disconnected');
	$('#plot').removeClass('plot-disconnected');
	googleAnalytics.sendEvent('Connect', 'Click');
    updateLeftPanelView(true);
    refreshConnectButton();
}

function onClosed(result) {
    if (result) { // All went as expected
        GUI.log(getMessage('serialPortClosedOk'));
    } else { // Something went wrong
        GUI.log(getMessage('serialPortClosedFail'));
    }

	$('#data-pane #hardware').html(""); 
	$('#left-pane-tare-button a').off().hide(); //Hide the tare button in the left pane.
	$('#left-pane-tare-airspeed-button a').off().hide(); //Hide the tare button in the left pane.
    $('#left-pane-tare-current-button a').off().hide(); //Hide the tare button in the left pane.
    $('#tabs ul.mode-connected').addClass('tab-disconnected');
	$('#plot').addClass('plot-disconnected'); //
  
    //delete CONFIG.boardVersion;
    delete CONFIG.displayVersion;
  
    clearAutoConsole(); //clears the automatic control console
    
    var documentationButton = $('#button-documentation');
    documentationButton.hide();

    display_text();
    clearInterval(USER_LIMITS.refreshRate);
    //$('.tare-buttons').hide();
    refreshConnectButton();
}

function read_serial(info) {
        MSP.read(info);
}

function sensor_status(sensors_detected) {
    // initialize variable (if it wasn't)
    if (!sensor_status.previous_sensors_detected) {
        sensor_status.previous_sensors_detected = 0;
    }

    // update UI (if necessary)
    if (sensor_status.previous_sensors_detected == sensors_detected) {
        return;
    }
    
    // set current value
    sensor_status.previous_sensors_detected = sensors_detected;

    var e_sensor_status = $('div#sensor-status');

    if (have_sensor(sensors_detected, 'acc')) {
        $('.accel', e_sensor_status).addClass('on');
    } else {
        $('.accel', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'gyro')) {
        $('.gyro', e_sensor_status).addClass('on');
    } else {
        $('.gyro', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'baro')) {
        $('.baro', e_sensor_status).addClass('on');
    } else {
        $('.baro', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'mag')) {
        $('.mag', e_sensor_status).addClass('on');
    } else {
        $('.mag', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'gps')) {
        $('.gps', e_sensor_status).addClass('on');
    } else {
        $('.gps', e_sensor_status).removeClass('on');
    }

    if (have_sensor(sensors_detected, 'sonar')) {
        $('.sonar', e_sensor_status).addClass('on');
    } else {
        $('.sonar', e_sensor_status).removeClass('on');
    }
}

function have_sensor(sensors_detected, sensor_code) {
    switch(sensor_code) {
        case 'acc':
        case 'gyro':
            return bit_check(sensors_detected, 0);
        case 'baro':
            return bit_check(sensors_detected, 1);
        case 'mag':
            return bit_check(sensors_detected, 2);
        case 'gps':
            return bit_check(sensors_detected, 3);
        case 'sonar':
            return bit_check(sensors_detected, 4);
    }
    return false;
}

function highByte(num) {
    return num >> 8;
}

function lowByte(num) {
    return 0x00FF & num;
}

function specificByte(num, pos) {
    return 0x000000FF & (num >> (8 * pos));
}

function bit_check(num, bit) {
    return ((num >> bit) % 2 != 0);
}

function bit_set(num, bit) {
    return num | 1 << bit;
}

function bit_clear(num, bit) {
    return num & ~(1 << bit);
}

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.uncapitalizeFirstLetter = function() {
    return this.charAt(0).toLowerCase() + this.slice(1);
}