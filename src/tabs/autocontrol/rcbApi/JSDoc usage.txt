1) install JSDoc
:

npm install -g jsdoc



2) open cmd and navigate to this folder



3) generate the documentation using minami theme
:

jsdoc rcbApi.js -t "minami/"

4) Check the docs were generated correctly and update as needed (delete the "out" folder before regenerating)

3) Rename the generated "out" folder to the new version, eg "v12"

4) upload the updated documentation to online server (google cloud bucket). 
DO NOT OVERWRITE OLD VERSIONS, poeple using old apps will have links to the old docu!!!

5) update the app to the new doc's version by changing its url

6) delete the generated docs (do not commit)