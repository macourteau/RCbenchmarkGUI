'use strict';

TABS.utilities = {};

//Called by the scripting function as well
function readOhmmeter(callback){
	googleAnalytics.sendEvent('Feature', 'Using', 'Ohmmeter');

	//poll the status until good
	GUI.interval_add('ohmmeter', function () {
		MSP.send_message(MSP_codes.MSP_GETOHM, false, false, function() {
			if(SENSOR_DATA.ohmStatus >= 2){
				GUI.interval_remove('ohmmeter');
				if(callback)callback(SENSOR_DATA.ohmReading);
			}
		});
	}, 100, true);
}

// This function will add the html for the pressure sensor if it is available.
function htmlPressureSensor()
{
    if(DATA.isPressureSensorAvailable()){
        $("#no-airspeed-probes-display").hide();
        $("#airspeed-probes-display").show();       
    }
    else {
        $("#no-airspeed-probes-display").show();
        $("#airspeed-probes-display").hide();
    }
}

TABS.utilities.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'utilities') {
        GUI.active_tab = 'utilities';
        googleAnalytics.sendAppView('Utilities');
    }

    $('#tabContent').load("./tabs/utilities.html", function () {        
        showBoardSpecific(false);
        htmlPressureSensor();
        
        // *** START OF CONTROL BOARD SECTION ***
      
        // Output min and max section
        var ESCCutoffGUI = $('.tab-utilities #ESCCutoff');
        var ESCCutoffGUIB = $('.tab-utilities #ESCCutoffB');
        var pwm1MinGUI = $('.tab-utilities #PWM1Min');
        var pwm1MaxGUI = $('.tab-utilities #PWM1Max');
        var pwm2MinGUI = $('.tab-utilities #PWM2Min');
        var pwm2MaxGUI = $('.tab-utilities #PWM2Max');
        var pwm3MinGUI = $('.tab-utilities #PWM3Min');
        var pwm3MaxGUI = $('.tab-utilities #PWM3Max');
        var pwm4MinGUI = $('.tab-utilities #PWM4Min');
        var pwm4MaxGUI = $('.tab-utilities #PWM4Max');
      
        // Make sure the cutoff is within the range allowed
        function updateESCCutoff(step, minPWMTime, maxPWMTime) {
            if( minPWMTime >  Number(ESCCutoffGUI.val())){
              ESCCutoffGUI.val(minPWMTime);
            }  
            if( Number(ESCCutoffGUI.val()) > maxPWMTime){
              ESCCutoffGUI.val(maxPWMTime);
            }

            if(CONFIG.boardVersion === "Series 1780"){
              if( Number(pwm3MinGUI.val()) >  Number(ESCCutoffGUIB.val())){
                ESCCutoffGUIB.val(pwm3MinGUI.val());
              }  
              if( Number(ESCCutoffGUIB.val()) >  Number(pwm3MaxGUI.val())){
                ESCCutoffGUIB.val(pwm3MaxGUI.val());
              }
              changeMemory(CONFIG.ESCCutoffA, Number(ESCCutoffGUI.val()));
              changeMemory(CONFIG.ESCCutoffB, Number(ESCCutoffGUIB.val()));
            }else{
              changeMemory(CONFIG.ESCCutoff, Number(ESCCutoffGUI.val()));
            }
            
            ESCCutoffGUI.attr({
                step: step,
                min: minPWMTime,
                max: maxPWMTime
            });
            ESCCutoffGUIB.attr({
                step: step,
                min: minPWMTime,
                max: maxPWMTime
            });
        }
      
        function outputRangeGui(step, minGUI, maxGUI, isMin, savedValue, minPWMTime, maxPWMTime) {
            var currentGUI;
            if (isMin) {
                currentGUI = minGUI;
            }
            else {
                currentGUI = maxGUI;
            }
            currentGUI.attr({
                min: minPWMTime,
                max: maxPWMTime,
                step: step
            });
            currentGUI.val(savedValue.value);
            currentGUI.change(function () {
                if (isMin) {
                    if  ( Number(minGUI.val()) >  Number(maxGUI.val()) ) {
                        minGUI.val(maxGUI.val());
                    }
                    minLimitsGUI(minGUI, maxGUI);
                }
                else {
                    if  ( Number(minGUI.val()) >  Number(maxGUI.val()) ) {
                        maxGUI.val(minGUI.val());
                    }
                    maxLimitsGUI(minGUI, maxGUI);
                }
                changeMemory(savedValue, Number(currentGUI.val()))
            });
        }
      
        function refreshControlBoard() {          
            var protocol = TABS.motors.getControlProtocol();
            
            var us = "";
            if(CONTROL_PROTOCOLS[protocol].microseconds){
              us = " (&micro;s)";
            }
          
            if(CONFIG.controlBoard_flag_active){
              $(".control_board").show();
              $(".no_control_board").hide();
              $("#selectControlProtocol option").prop("disabled", false);

              // Put labels for the output limits
              if(CONFIG.boardVersion === "Series 1580"){
                $('.tab-utilities #PWM1Title').html(getMessage('sliderESC') + us);        
                $('.tab-utilities #PWM2Title').html(getMessage('sliderCh1') + us);   
                $('.tab-utilities #PWM3Title').html(getMessage('sliderCh2') + us);   
                $('.tab-utilities #PWM4Title').html(getMessage('sliderCh3') + us);   
              }else{
                alert("Control board only compatible with Series 1580/1585");
              }
              /*if(CONFIG.boardVersion === "Series 1780"){
                if(CONFIG.s1780detected.Bside){
                  $('.tab-utilities #PWM1Title').html(getMessage('sliderESCA') + " (&micro;s)");        
                  $('.tab-utilities #PWM2Title').html(getMessage('sliderServoA') + " (&micro;s)"); 
                  $('.tab-utilities #PWM3Title').html(getMessage('sliderESCB') + " (&micro;s)");        
                  $('.tab-utilities #PWM4Title').html(getMessage('sliderServoB') + " (&micro;s)"); 
                }else{
                  $('.tab-utilities #PWM1Title').html(getMessage('sliderESC') + " (&micro;s)");        
                  $('.tab-utilities #PWM2Title').html(getMessage('sliderServo') + " (&micro;s)"); 
                }
              }*/
            }else{
              $(".control_board").hide();
              $(".no_control_board").show();
              $("#selectControlProtocol option").prop("disabled", true);
              $("#selectControlProtocol option#pwm_50").prop("disabled", false);

              // Put labels for the output limits
              if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
                $('.tab-utilities #PWM1Title').html(getMessage('sliderESC') + us);        
                $('.tab-utilities #PWM2Title').html(getMessage('sliderServo1') + us);   
                $('.tab-utilities #PWM3Title').html(getMessage('sliderServo2') + us);   
                $('.tab-utilities #PWM4Title').html(getMessage('sliderServo3') + us);   
              }  
              if(CONFIG.boardVersion === "Series 1780"){
                if(CONFIG.s1780detected.Bside){
                  $('.tab-utilities #PWM1Title').html(getMessage('sliderESCA') + us);        
                  $('.tab-utilities #PWM2Title').html(getMessage('sliderServoA') + us); 
                  $('.tab-utilities #PWM3Title').html(getMessage('sliderESCB') + us);        
                  $('.tab-utilities #PWM4Title').html(getMessage('sliderServoB') + us); 
                }else{
                  $('.tab-utilities #PWM1Title').html(getMessage('sliderESC') + us);        
                  $('.tab-utilities #PWM2Title').html(getMessage('sliderServo') + us); 
                }
              }
            }
            
            //Update the limits
            var minPWMTime = CONTROL_PROTOCOLS[protocol].min_val;
            var maxPWMTime = CONTROL_PROTOCOLS[protocol].max_val;
            $("#cb_min_supported_range").html(minPWMTime);
            $("#cb_max_supported_range").html(maxPWMTime);
          
            //calculate the step size
            var step = (maxPWMTime-minPWMTime) / (CONTROL_PROTOCOLS[protocol].msp_max - CONTROL_PROTOCOLS[protocol].msp_min);
            step = TABS.motors.pwmPretty(step);

            if(CONFIG.boardVersion === "Series 1780"){
              ESCCutoffGUI.val(TABS.motors.pwmPretty(CONFIG.ESCCutoffA.value));
              ESCCutoffGUIB.val(TABS.motors.pwmPretty(CONFIG.ESCCutoffB.value));
            }else{
              ESCCutoffGUI.val(TABS.motors.pwmPretty(CONFIG.ESCCutoff.value));
            }
            
            if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
              outputRangeGui(step, pwm1MinGUI, pwm1MaxGUI, true, CONFIG.ESCMin, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm1MinGUI, pwm1MaxGUI, false, CONFIG.ESCMax, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm2MinGUI, pwm2MaxGUI, true, CONFIG.servo1Min, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm2MinGUI, pwm2MaxGUI, false, CONFIG.servo1Max, minPWMTime, maxPWMTime);     
              outputRangeGui(step, pwm3MinGUI, pwm3MaxGUI, true, CONFIG.servo2Min, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm3MinGUI, pwm3MaxGUI, false, CONFIG.servo2Max, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm4MinGUI, pwm4MaxGUI, true, CONFIG.servo3Min, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm4MinGUI, pwm4MaxGUI, false, CONFIG.servo3Max, minPWMTime, maxPWMTime);
            }  
            if(CONFIG.boardVersion === "Series 1780"){
              outputRangeGui(step, pwm1MinGUI, pwm1MaxGUI, true, CONFIG.ESCAMin, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm1MinGUI, pwm1MaxGUI, false, CONFIG.ESCAMax, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm2MinGUI, pwm2MaxGUI, true, CONFIG.ServoAMin, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm2MinGUI, pwm2MaxGUI, false, CONFIG.ServoAMax, minPWMTime, maxPWMTime);     
              outputRangeGui(step, pwm3MinGUI, pwm3MaxGUI, true, CONFIG.ESCBMin, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm3MinGUI, pwm3MaxGUI, false, CONFIG.ESCBMax, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm4MinGUI, pwm4MaxGUI, true, CONFIG.ServoBMin, minPWMTime, maxPWMTime);
              outputRangeGui(step, pwm4MinGUI, pwm4MaxGUI, false, CONFIG.ServoBMax, minPWMTime, maxPWMTime);
            } 
          
            $("#selectControlProtocol").val(protocol);
            updateESCCutoff(step, minPWMTime, maxPWMTime);
            refreshCutoffs();
		}
      
        // Setup callbacks when inputs changed
        $('#selectControlProtocol').change(function(){
          var newProtocol = $("#selectControlProtocol").val();
          TABS.motors.controlBoardSet(newProtocol, refreshControlBoard);
        });
        ESCCutoffGUI.change(function(){
          if(CONFIG.boardVersion === "Series 1780"){
            changeMemory(CONFIG.ESCCutoffA, ESCCutoffGUI.val());
          }else{
            changeMemory(CONFIG.ESCCutoff, ESCCutoffGUI.val());
          }
          refreshControlBoard();
        });
        ESCCutoffGUIB.change(function(){
          changeMemory(CONFIG.ESCCutoffB, ESCCutoffGUIB.val());
          refreshControlBoard();
        });
      
        refreshControlBoard();
      
        $("#infoBuyControlBoard").attr("title",getMessage("controlBoardPromo"));
        $("#infoBuyControlBoard").tooltip(); //pretty display
        
        // ***** END OF CONTROL BOARD SECTION ****

        // ***** START OF CURRENT CALIBRATION SECTION *****

        $(".tab-utilities #info-current-calibration").attr("title",getMessage("currentCalibrationHelp"));
        $(".tab-utilities #info-current-calibration").tooltip(); //pretty display
        var currentScaleFactor = $('.tab-utilities #current-scale-factor')
        currentScaleFactor.val(CONFIG.currentScale15xx.value)
        currentScaleFactor.change(function(){
            changeMemory(CONFIG.currentScale15xx, $( this ).val())
        })

        //var tareCurrentBtn = $(".tab-utilities #current-tare-button-15xx");
        var tareCurrentBtnText = $(".tab-utilities #current-tare-button-15xx > a");
        tareCurrentBtnText.on('click', function (){
            tareCurrentBtnText.prop("disabled", true);
            tareCurrentBtnText.text(getMessage('pleaseWait'))
            DATA.tareCurrent(function(){
                tareCurrentBtnText.prop("disabled", false);
                tareCurrentBtnText.text(getMessage("tareCurrent"))
            });
        });

        $(".tab-utilities #current-calibration-reset-button-15xx > a").on('click', function (){
            console.log("Setting current calibration to factory default")
            changeMemory(CONFIG.currentScale15xx, 1.0);
            changeMemory(CONFIG.currentTare15xx, 0.0);
            currentScaleFactor.val(1.0)
            $(".tab-utilities #factoryDefaultCurrentDone").text(getMessage("done"))
        })

        // ***** END OF CURRENT CALIBRATION SECTION ******
        
        
        // handle the reverse checkboxes
        $(".tab-utilities .section .reverse_chk #reverse_thrust").prop( "checked", CONFIG.reverseThrust.value);
        $(".tab-utilities .section .reverse_chk #reverse_torque").prop( "checked", CONFIG.reverseTorque.value);
        $(".tab-utilities .section .reverse_chk #reverse_thrustB").prop( "checked", CONFIG.reverseThrustB.value);
        $(".tab-utilities .section .reverse_chk #reverse_torqueB").prop( "checked", CONFIG.reverseTorqueB.value);
        $(".tab-utilities .section .reverse_chk #reverse_thrust").change(function(){
            var checked = $(this).is(":checked");
            changeMemory(CONFIG.reverseThrust, checked);
            googleAnalytics.sendEvent('ReverseThrust', 'Value', checked);
        });
        $(".tab-utilities .section .reverse_chk #reverse_torque").change(function(){
            var checked = $(this).is(":checked");
            changeMemory(CONFIG.reverseTorque, checked);
            googleAnalytics.sendEvent('ReverseTorque', 'Value', checked);
        });
        $(".tab-utilities .section .reverse_chk #reverse_thrustB").change(function(){
            var checked = $(this).is(":checked");
            changeMemory(CONFIG.reverseThrustB, checked);
            googleAnalytics.sendEvent('ReverseThrustB', 'Value', checked);
        });
        $(".tab-utilities .section .reverse_chk #reverse_torqueB").change(function(){
            var checked = $(this).is(":checked");
            changeMemory(CONFIG.reverseTorqueB, checked);
            googleAnalytics.sendEvent('ReverseTorqueB', 'Value', checked);
        });
        
        // handle CSV notes
        $(".tab-utilities #csv-notes-text").val(CONFIG.csvNotes.value);
        if(CONFIG.csvNotes.value && CONFIG.csvNotes.value !== ''){
            console.log("Loaded: ");
            console.log(CONFIG.csvNotes.value);
            googleAnalytics.sendEvent('Feature', 'Using', 'CSV-Notes');
        }
        $(".tab-utilities #csv-notes-text").on('input',function(){
            var notes = $(".tab-utilities #csv-notes-text").val();
            changeMemory(CONFIG.csvNotes, notes);
        });
        $(".tab-utilities #csv-notes-tooltip").tooltip({ show: { effect: "blind", duration: 100 } });
		
		// function to execute when GetOhm button is clicked
		var getOhmButton = $('#utilities-read-ohm-button .get-ohm-button');
		getOhmButton.on('click', function(){
			getOhmButton.text(getMessage('sensorsButtonGettingOhm'));
            $("#ohmmeter-result").html("");
			readOhmmeter(function(value){
                if(value>200){
                  value = 200;
                  SENSOR_DATA.ohmStatus = 4;
                }
				var displayValue = value.toPrecision(4).toString() + ' Ohm';
				if(SENSOR_DATA.ohmStatus == 3){
					displayValue = '~' + displayValue;
				}
				if(SENSOR_DATA.ohmStatus == 4){
					displayValue = '>' + displayValue;
				}
				getOhmButton.text(getMessage("readOhm"));
                $("#ohmmeter-result").html(displayValue);
			});
		});
		
		//calibration
		function showCalibWizard(wizardId){            
			$('#showCalibWizard').load("tabs/calib_wizard.html", function () {
                var popupTitle = "";
                if(wizardId === "thrust")
                    popupTitle = getMessage('wizardThrustCalibTitle');
                if(wizardId === "torque")
                    popupTitle = getMessage('wizardTorqueCalibTitle');
                $('#showCalibWizard').dialog({
                    show: {
                        effect: "blind",
                        duration: 500
                    },
                    hide: {
                        effect: "blind",
                        duration: 500
                    },
                    title: popupTitle,
                    resizable: true,
                    draggable: true,
                    height: 600,
                    width: 600,
                    dialogClass: 'ui-dialog-osx',
                    modal: true,
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
                });
                calib_wizard(wizardId);
            });
		}
		//$('#content .tab-utilities a.calibrateDyn').click(function () {
		$('#calibrate-torque-weight-button .button').on('click', function(){
			googleAnalytics.sendEvent('CalibTorque', 'Click');
            showCalibWizard('torque');
        });
        
		//$('#content .tab-utilities a.calibrateDynThrust').click(function () {
		$('#calibrate-thrust-button .button').on('click', function(){
			googleAnalytics.sendEvent('CalibThrust', 'Click');
            showCalibWizard('thrust');
        });
		
        
        $(".pitotInfoButton").click(function(){
            console.log(2);
            $('#pitotShowDialog').load("../pressureSensor.html", function (content) {
				translateAll();
                $('#pitotShowDialog').dialog({
                    show: {
                        effect: "blind",
                        duration: 500
                    },
                    hide: {
                        effect: "blind",
                        duration: 500
                    },
                    resizable: true,
                    draggable: true,
                    height: 600,
                    width: 650,
                    dialogClass: 'ui-dialog-osx',
                    modal: true,
                    open: function () {
                        $(this).parent().promise().done(function () {
                            $(".ui-dialog-content").scrollTop(0);
                        });
                    },
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
                });
            });
            googleAnalytics.sendEvent('PitotHelp', 'Click');
        })
        
        
	    $('.tab-utilities .utilities-left .section .ESCCutoff .info').html('<img id="infoTag"src="images/question.png" alt="Pole Info" i18n_title="escCutOffTip" height="20" width="20">');              
        
        $(".tab-utilities .utilities-left .section .ESCCutoff .info > img").tooltip({ show: { effect: "blind", duration: 100 } });
        
		handleCalibration();     
        
        // The following section handles the change of value in the interface.
        function bindValue2Jq (jqHandle, memLocation, displayPrecision, minVal, maxVal) {
            // Jquery handle calibration coefficient
            // Set GUI value initial load
            jqHandle.val(parseFloat(memLocation.value).toFixed(displayPrecision));
            // If there is a change in the GUI        
            function changeValueInMem() {
                var val = jqHandle.val();
                val = Math.min(Math.max(minVal, val), maxVal);
                jqHandle.val(val);
                changeMemory(memLocation, val);
            }
            jqHandle.change( changeValueInMem );
        }
        
        bindValue2Jq ($('.tab-utilities #calibrationCoefficient'), CONFIG.pressureCal, 3, 0.01, 2);  
        bindValue2Jq ($('.tab-utilities #atmPressure'), CONFIG.atmPressure, 0, 85000, 106500);
        
        translateAll();
		if (callback) callback();
	});
};

TABS.utilities.cleanup = function (callback) {
    if (callback) callback();
};