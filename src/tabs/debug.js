'use strict';

TABS.debug = {};
TABS.debug.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'debug') {
        GUI.active_tab = 'debug';
        googleAnalytics.sendAppView('Debug');
    }

    $('#tabContent').load("./tabs/debug.html", function () {
        // translate to user-selected language
        translateAll();
        
        // save an updated system limit
        function saveHacks(){
          //ensure burst current is at least equal to the max continuous current
          //IMPLEMENTED LATER...
          /*if(HACKS.s15xx.currentBurstMax < HACKS.s15xx.currentMax){
            HACKS.s15xx.currentBurstMax = HACKS.s15xx.currentMax;
          }
          if(HACKS.s15xx.currentBurstMin > HACKS.s15xx.currentMin){
            HACKS.s15xx.currentBurstMin = HACKS.s15xx.currentMin;
          }
          if(HACKS.s1780.currentBurstMaxA < HACKS.s1780.currentMaxA){
            HACKS.s15xx.currentBurstMax = HACKS.s15xx.currentMax;
          }
          if(HACKS.s15xx.currentBurstMin > HACKS.s15xx.currentMin){
            HACKS.s15xx.currentBurstMin = HACKS.s15xx.currentMin;
          }*/
          
          chrome.storage.local.set({'HACKS': HACKS});
          refreshCutoffs();
        }
        
        /* Setup hack codes
           - Hack codes are only shared by email upon customer request. 
           - Use the lowest increase that will satisfy the customer needs.
           - If you are getting these codes by reading this source code 
             or from anywhere else, do so AT YOUR OWN RISKS! 
           - Using these codes voids the warranty on our products!
           - To create new codes: https://passwordsgenerator.net/?length=6&symbols=0&numbers=1&lowercase=1&uppercase=1&similar=1&ambiguous=0&client=1&autoselect=0
           - Make sure to delete the 6th caracter from the password generated above, because we only use 5 chars.
        */
        $('.tab-debug #debugHack').on( "change", function applyHack(){          
            var pass = "";
            var fail = "";
            var warrantyVoid = true;
            if(HACKS === undefined){
              HACKS = {};
            }
            if(HACKS.s15xx === undefined){
              HACKS.s15xx = {};
            }
            if(HACKS.s1780 === undefined){
              HACKS.s1780 = {};
            }
          
            switch($(this).val()) {
                //**** RESET ****
                //Reset all hack codes
                case 'reset':
                    //SYSTEM_LIMITS_15xx = $.extend(true, {}, ORIGINAL_SYSTEM_LIMITS_15xx);
                    //SYSTEM_LIMITS_1780 = $.extend(true, {}, ORIGINAL_SYSTEM_LIMITS_1780);
                    delete HACKS.s15xx;
                    delete HACKS.s1780;
                    pass = getMessage("removedAllHackCodes");
                    warrantyVoid = false;
                    break;   
                
                //**** ACTIONS ****
                //Special commands that trigger debug actions
                case 'ZrXxD':
                    fail = "Deprecated. To tare the current, use the current calibration section in the 'Utilities' tab.";
                    warrantyVoid = false;
                    break;
                
                //Erase the local memory (simulates a clean install);
                case 'clear':
                    pass = "Clearing the application cache... App should restart, if not do it manually.";
                    warrantyVoid = false;
                    clearAppStorage();
                    break;
                
                //**** THRUST ****
                //Increase thrust limit to ±5kg for Series 15xx
                case 'stPhv':
                    HACKS.s15xx.thrustMax = 5;
                    HACKS.s15xx.thrustMin = -5;
                    pass = getMessage("changeMaximumThrustTokgForSeries15xx",[5]);
                    break;
                //Increase thrust limit to ±10kg for Series 15xx
                case 'bPhNN':
                    HACKS.s15xx.thrustMax = 10;
                    HACKS.s15xx.thrustMin = -10;
                    pass = getMessage("changeMaximumThrustTokgForSeries15xx",[10]);
                    break;
                //Increase thrust limit to ±100kg for Series 15xx
                case 'UdUsa':
                    HACKS.s15xx.thrustMax = 100;
                    HACKS.s15xx.thrustMin = -100;
                    pass = getMessage("changeMaximumThrustTokgForSeries15xx",[100]);
                    break;     
                //Increase thrust limit to ±25kg for Series 1780
                case 'DaqjK':
                    HACKS.s1780.thrustMaxA = 25;
                    HACKS.s1780.thrustMinA = -25;
                    HACKS.s1780.thrustMaxB = 25;
                    HACKS.s1780.thrustMinB = -25;
                    pass = getMessage("changeMaximumThrustTokgForSeries1780",[25]);
                    break;     
                //Increase thrust limit to ±30kg for Series 1780
                case 'hQdGg':
                    HACKS.s1780.thrustMaxA = 30;
                    HACKS.s1780.thrustMinA = -30;
                    HACKS.s1780.thrustMaxB = 30;
                    HACKS.s1780.thrustMinB = -30;
                    pass = getMessage("changeMaximumThrustTokgForSeries1780",[30]);
                    break;  
                //Increase thrust limit to ±35kg for Series 1780
                case 'nKasr':
                    HACKS.s1780.thrustMaxA = 35;
                    HACKS.s1780.thrustMinA = -35;
                    HACKS.s1780.thrustMaxB = 35;
                    HACKS.s1780.thrustMinB = -35;
                    pass = getMessage("changeMaximumThrustTokgForSeries1780",[35]);
                    break;  
                //Increase thrust limit to ±40kg for Series 1780
                case 'AhCue':
                    HACKS.s1780.thrustMaxA = 40;
                    HACKS.s1780.thrustMinA = -40;
                    HACKS.s1780.thrustMaxB = 40;
                    HACKS.s1780.thrustMinB = -40;
                    pass = getMessage("changeMaximumThrustTokgForSeries1780",[40]);
                    break; 
                //Increase thrust limit to +-50kg for Series 1780
                case '6Pwsg':
                    HACKS.s1780.thrustMaxA = 50;
                    HACKS.s1780.thrustMinA = -50;
                    HACKS.s1780.thrustMaxB = 50;
                    HACKS.s1780.thrustMinB = -50;
                    pass = "Changed maximum thrust to ±50kg for Series 1780.";
                    break; 
                //Increase thrust limit to ±70kg for Series 1780
                case 'qKcSk':
                    HACKS.s1780.thrustMaxA = 70;
                    HACKS.s1780.thrustMinA = -70;
                    HACKS.s1780.thrustMaxB = 70;
                    HACKS.s1780.thrustMinB = -70;
                    pass = "Changed maximum thrust to ±70kg for Series 1780.";
                    break; 
                //Increase thrust limit to ±100kg for Series 1780
                case 'cgd9M':
                    HACKS.s1780.thrustMaxA = 100;
                    HACKS.s1780.thrustMinA = -100;
                    HACKS.s1780.thrustMaxB = 100;
                    HACKS.s1780.thrustMinB = -100;
                    pass = "Changed maximum thrust to ±70kg for Series 1780.";
                    break; 
                //Increase thrust limit to ±105kg for Series 1780
                case 'St5fP':
                    HACKS.s1780.thrustMaxA = 105;
                    HACKS.s1780.thrustMinA = -105;
                    HACKS.s1780.thrustMaxB = 105;
                    HACKS.s1780.thrustMinB = -105;
                    pass = "Changed maximum thrust to ±105kg for Series 1780.";
                    break; 
                //Increase thrust limit to ±135kg for Series 1780
                case 'F3zNQ':
                    HACKS.s1780.thrustMaxA = 135;
                    HACKS.s1780.thrustMinA = -135;
                    HACKS.s1780.thrustMaxB = 135;
                    HACKS.s1780.thrustMinB = -135;
                    pass = "Changed maximum thrust to ±135kg for Series 1780.";
                    break; 
                //Increase thrust limit to ±150kg for Series 1780
                case '4MUgQ':
                    HACKS.s1780.thrustMaxA = 150;
                    HACKS.s1780.thrustMinA = -150;
                    HACKS.s1780.thrustMaxB = 150;
                    HACKS.s1780.thrustMinB = -150;
                    pass = "Changed maximum thrust to ±150kg for Series 1780.";
                    break; 
                //Increase thrust limit to ±300kg for Series 1780
                case 'c69DY':
                    HACKS.s1780.thrustMaxA = 300;
                    HACKS.s1780.thrustMinA = -300;
                    HACKS.s1780.thrustMaxB = 300;
                    HACKS.s1780.thrustMinB = -300;
                    pass = "Changed maximum thrust to ±300kg for Series 1780.";
                    break; 
                    
                //**** TORQUE ****
                //Increase torque limit to ±4Nm for Series 15xx
                case 'HCsFp':
                    HACKS.s15xx.torqueMax = 4;
                    HACKS.s15xx.torqueMin = -4;
                    pass = getMessage("changeMaximumTorqueTokgForSeries15xx",[4]);
                    break;
                //Increase torque limit to ±10Nm for Series 15xx
                case '9D7fT':
                    HACKS.s15xx.torqueMax = 10;
                    HACKS.s15xx.torqueMin = -10;
                    pass = getMessage("changeMaximumTorqueTokgForSeries15xx",[10]);
                    break;
                //Increase torque limit to ±12Nm for Series 1780
                case 'rnXuT':
                    HACKS.s1780.torqueMaxA = 12;
                    HACKS.s1780.torqueMinA = -12;
                    HACKS.s1780.torqueMaxB = 12;
                    HACKS.s1780.torqueMinB = -12;
                    pass = getMessage("changeMaximumTorqueTokgForSeries1780",[12]);
                    break; 
                //Increase torque limit to ±16.5Nm for Series 1780 (max supported by load cells in theory)
                case 'Khets':
                    HACKS.s1780.torqueMaxA = 16.5;
                    HACKS.s1780.torqueMinA = -16.5;
                    HACKS.s1780.torqueMaxB = 16.5;
                    HACKS.s1780.torqueMinB = -16.5;
                    pass = getMessage("changeMaximumTorqueTokgForSeries1780",[16.5]);
                    break; 
                //Increase torque limit to ±20Nm for Series 1780
                case 'oQJkK':
                    HACKS.s1780.torqueMaxA = 20;
                    HACKS.s1780.torqueMinA = -20;
                    HACKS.s1780.torqueMaxB = 20;
                    HACKS.s1780.torqueMinB = -20;
                    pass = getMessage("changeMaximumTorqueTokgForSeries1780",[20]);
                    break;                 
                //Increase torque limit to ±22Nm for Series 1780
                case 'XFE5T':
                    HACKS.s1780.torqueMaxA = 22;
                    HACKS.s1780.torqueMinA = -22;
                    HACKS.s1780.torqueMaxB = 22;
                    HACKS.s1780.torqueMinB = -22;
                    pass = "Changed maximum torque to ±22Nm for Series 1780.";
                    break; 
                //Increase torque limit to ±23Nm for Series 1780
                case 'h38Aj':
                    HACKS.s1780.torqueMaxA = 23;
                    HACKS.s1780.torqueMinA = -23;
                    HACKS.s1780.torqueMaxB = 23;
                    HACKS.s1780.torqueMinB = -23;
                    pass = "Changed maximum torque to ±23Nm for Series 1780.";
                    break; 
                //Increase torque limit to ±30Nm for Series 1780
                case 'gbV4b':
                    HACKS.s1780.torqueMaxA = 30;
                    HACKS.s1780.torqueMinA = -30;
                    HACKS.s1780.torqueMaxB = 30;
                    HACKS.s1780.torqueMinB = -30;
                    pass = "Changed maximum torque to ±30Nm for Series 1780.";
                    break;     
                //Increase torque limit to ±50Nm for Series 1780
                case 'V7j7M':
                    HACKS.s1780.torqueMaxA = 50;
                    HACKS.s1780.torqueMinA = -50;
                    HACKS.s1780.torqueMaxB = 50;
                    HACKS.s1780.torqueMinB = -50;
                    pass = "Changed maximum torque to ±50Nm for Series 1780.";
                    break;   
                //Increase torque limit to ±70Nm for Series 1780
                case 'aqD2G':
                    HACKS.s1780.torqueMaxA = 70;
                    HACKS.s1780.torqueMinA = -70;
                    HACKS.s1780.torqueMaxB = 70;
                    HACKS.s1780.torqueMinB = -70;
                    pass = "Changed maximum torque to ±70Nm for Series 1780.";
                    break;   
                //Increase torque limit to ±75Nm for Series 1780
                case '8kH7w':
                    HACKS.s1780.torqueMaxA = 75;
                    HACKS.s1780.torqueMinA = -75;
                    HACKS.s1780.torqueMaxB = 75;
                    HACKS.s1780.torqueMinB = -75;
                    pass = "Changed maximum torque to ±75Nm for Series 1780.";
                    break;   
                //Increase torque limit to ±100Nm for Series 1780
                case 'SBm7q':
                    HACKS.s1780.torqueMaxA = 100;
                    HACKS.s1780.torqueMinA = -100;
                    HACKS.s1780.torqueMaxB = 100;
                    HACKS.s1780.torqueMinB = -100;
                    pass = "Changed maximum torque to ±100Nm for Series 1780.";
                    break;  
                //Increase torque limit to ±200Nm for Series 1780
                case 'Cbq4Y':
                    HACKS.s1780.torqueMaxA = 200;
                    HACKS.s1780.torqueMinA = -200;
                    HACKS.s1780.torqueMaxB = 200;
                    HACKS.s1780.torqueMinB = -200;
                    pass = "Changed maximum torque to ±200Nm for Series 1780.";
                    break;  
                
                //**** CALIBRATION ****
                //Calibration always passes (for using custom load cells) for Series 15xx
                case 'kGL7Lj':
                    HACKS.s15xx.calibrationAlwaysPass = true;
                    pass = getMessage("calibrationWillNowAlwaysPassForSeries15xx");
                    break;
                    
                //**** LOAD CELL OVERLOAD CUTOFF ****
                //Disable the load cell overload cutoff for Series 1780
                case 'ERghH':
                    HACKS.s1780.disableLoadCellOverloadProtection = true;
                    pass = getMessage("loadCellOverloadProtectionIsNowDisabledForSeries1780");
                    break;
                
                //**** MECHANICAL RPM ****
                //Increase rpm limit to 500000 erpm for Series 15xx
                case 'Qux2e':
                    HACKS.s15xx.erpmMax = 500000;
                    pass = getMessage("maxERPMnow500000RPM");
                    break;
                  
                //**** CONTINUOUS CURRENT ****
                //Increase continuous current to ±60A for Series 15xx
                case 'yNx42':
                    HACKS.s15xx.currentMin = -60;
                    HACKS.s15xx.currentMax = 60;
                    pass = getMessage("changedContinuousCurrentToAforSeries15xx",[60]);
                    break;
                //Increase continuous current to ±70A for Series 15xx
                case 'CgADR':
                    HACKS.s15xx.currentMin = -70;
                    HACKS.s15xx.currentMax = 70;
                    pass = getMessage("changedContinuousCurrentToAforSeries15xx",[70]);
                    break;
                //Increase continuous current to ±100A for Series 15xx
                case 'AhJus':
                    HACKS.s15xx.currentMin = -100;
                    HACKS.s15xx.currentMax = 100;
                    pass = getMessage("changedContinuousCurrentToAforSeries15xx",[100]);
                    break;
                //Increase continuous current to ±150A for Series 15xx
                case 'i43Ck':
                    HACKS.s15xx.currentMin = -150;
                    HACKS.s15xx.currentMax = 150;
                    pass = getMessage("changedContinuousCurrentToAforSeries15xx",[150]);
                    break;
                //Increase continuous current to ±200A for Series 15xx -> DO NOT GO ABOVE THIS BEFORE HACKING THE FIRMWARE!
                case 'EZ3ir':
                    HACKS.s15xx.currentMin = -200;
                    HACKS.s15xx.currentMax = 200;
                    pass = getMessage("changedContinuousCurrentToAforSeries15xx",[200]);
                    break;
                //Increase continuous current to ±150A for Series 1780 for both sides
                case 'vkrwn':
                    HACKS.s1780.currentMinA = -150;
                    HACKS.s1780.currentMaxA = 150;
                    HACKS.s1780.currentMinB = -150;
                    HACKS.s1780.currentMaxB = 150;
                    pass = getMessage("changedContinuousCurrentTo150AforSeries1780");
                    break;
                //Increase continuous current to 200A for Series 1780
                case 'n4QtL':
                    //HACKS.s1780.currentMinA = -200;
                    HACKS.s1780.currentMaxA = 200;
                    //HACKS.s1780.currentMinB = -200;
                    HACKS.s1780.currentMaxB = 200;
                    pass = "Changed continuous current to 200A for Series 1780.";
                    break;
                //Increase continuous current to ±200A for Series 1780 for both sides
                case 'zPC3p':
                    HACKS.s1780.currentMinA = -200;
                    HACKS.s1780.currentMaxA = 200;
                    HACKS.s1780.currentMinB = -200;
                    HACKS.s1780.currentMaxB = 200;
                    pass = "Changed continuous current to ±200A for Series 1780.";
                    break;
                //Increase continuous current to ±250A for Series 1780 for both sides
                case 'QwY4j':
                    HACKS.s1780.currentMinA = -250;
                    HACKS.s1780.currentMaxA = 250;
                    HACKS.s1780.currentMinB = -250;
                    HACKS.s1780.currentMaxB = 250;                    
                    pass = "Changed continuous current to ±250A for Series 1780.";
                    break;
                    
                //**** BURST CURRENT ****
                //Increase burst current to ±80A for Series 15xx
                case 'SfWpP':
                    HACKS.s15xx.currentBurstMin = -80;
                    HACKS.s15xx.currentBurstMax = 80;
                    pass = getMessage("changedBurstCurrentToAforSeries15xx",[80]);
                    break;
                //Increase burst current to ±100A for Series 15xx
                case 'Tj4Qt':
                    HACKS.s15xx.currentBurstMin = -100;
                    HACKS.s15xx.currentBurstMax = 100;
                    pass = getMessage("changedBurstCurrentToAforSeries15xx",[100]);
                    break;
                //Increase burst current to ±150A for Series 15xx
                case 'p4fWU':
                    HACKS.s15xx.currentBurstMin = -150;
                    HACKS.s15xx.currentBurstMax = 150;
                    pass = getMessage("changedBurstCurrentToAforSeries15xx",[150]);
                    break;
                //Increase burst current to ±200A for Series 15xx -> DO NOT GO ABOVE THIS BEFORE HACKING THE FIRMWARE!
                case 'xzQKh':
                    HACKS.s15xx.currentBurstMin = -200;
                    HACKS.s15xx.currentBurstMax = 200;
                    pass = getMessage("changedBurstCurrentToAforSeries15xx",[200]);
                    break;
                //Increase burst current to ±150A for Series 1780 for both sides!
                case 'xseio':
                    HACKS.s1780.currentBurstMinA = -150;
                    HACKS.s1780.currentBurstMaxA = 150;
                    HACKS.s1780.currentBurstMinB = -150;
                    HACKS.s1780.currentBurstMaxB = 150;
                    pass = getMessage("changedBurstCurrentTo150AforSeries1780");
                    break;
                //Increase burst current to 200A for Series 1780!
                case 'rsg2b':
                    //HACKS.s1780.currentBurstMinA = -200;
                    HACKS.s1780.currentBurstMaxA = 200;
                    //HACKS.s1780.currentBurstMinB = -200;
                    HACKS.s1780.currentBurstMaxB = 200;
                    pass = "Changed burst current to 200A for Series 1780.";
                    break;
                //Increase burst current to ±200A for Series 1780 for both sides!
                case 'kUN9q':
                    HACKS.s1780.currentBurstMinA = -200;
                    HACKS.s1780.currentBurstMaxA = 200;
                    HACKS.s1780.currentBurstMinB = -200;
                    HACKS.s1780.currentBurstMaxB = 200;
                    pass = "Changed burst current to ±200A for Series 1780.";
                    break;
                //Increase burst current to ±300A for Series 1780 for both sides!
                case '5aBMw':
                    HACKS.s1780.currentBurstMinA = -300;
                    HACKS.s1780.currentBurstMaxA = 300;
                    HACKS.s1780.currentBurstMinB = -300;
                    HACKS.s1780.currentBurstMaxB = 300;
                    pass = "Changed burst current to ±300A for Series 1780.";
                    break;
                    
                //**** VOLTAGE ****
                //Increase voltage to 45V for Series 15xx
                case 'ur5bK':
                    HACKS.s15xx.voltageMax = 45;
                    pass = getMessage("changedMaxVoltageToVforSeries15xx",[45]);
                    break;
                //Increase voltage to 55V for Series 15xx -> DO NOT GO ABOVE THIS BEFORE HACKING THE FIRMWARE!
                case 'aGNmg':
                    HACKS.s15xx.voltageMax = 55;
                    pass = getMessage("changedMaxVoltageToVforSeries15xx",[55]);
                    break;
                //Increase voltage to 75V for Series 1780
                case 'HDXpC':
                    HACKS.s1780.voltageMaxA = 75;
                    HACKS.s1780.voltageMaxB = 75;
                    pass = getMessage("changedMaxVoltageTo75VforSeries 1780");
                    break;
                //Increase voltage to 100V for Series 1780
                case 'H8tCs':
                    HACKS.s1780.voltageMaxA = 100;
                    HACKS.s1780.voltageMaxB = 100;
                    pass = "Changed max voltage to 100V for Series 1780.";
                    break;
                //Increase voltage to 105V for Series 1780
                case 'g6DcA':
                    HACKS.s1780.voltageMaxA = 105;
                    HACKS.s1780.voltageMaxB = 105;
                    pass = "Changed max voltage to 105V for Series 1780.";
                    break;
                    
                //**** POWER ****
                //Increase power to 3kW for Series 15xx
                case 'QrB2y':
                    HACKS.s15xx.powerMax = 3000;
                    pass = getMessage("changedMaxPowerToKWforSeries15xx",[3000]);
                    break;
                //Increase power to 6kW for Series 15xx
                case 'Fz76d':
                    HACKS.s15xx.powerMax = 6000;
                    pass = getMessage("changedMaxPowerToKWforSeries15xx",[6000]);
                    break;
                //Increase power to 10kW for Series 15xx
                case 'ijDrw':
                    HACKS.s15xx.powerMax = 10000;
                    pass = getMessage("changedMaxPowerToKWforSeries15xx",[10000]);
                    break;
                //Increase power to 9kW for Series 1780
                case 'Hntsg':
                    HACKS.s1780.powerMaxA = 9000;
                    HACKS.s1780.powerMaxB = 9000;
                    pass =  getMessage("changedMaxPowerToKWforSeries1780",[9000]);
                    break;
                //Increase power to 11.25kW for Series 1780
                case 'vTHur':
                    HACKS.s1780.powerMaxA = 11250;
                    HACKS.s1780.powerMaxB = 11520;
                    pass = getMessage("changedMaxPowerToKWforSeries1780",[11250]);
                    break;
                //Increase power to 15kW for Series 1780
                case 'ZVQnM':
                    HACKS.s1780.powerMaxA = 15000;
                    HACKS.s1780.powerMaxB = 15000;
                    pass = "Changed max power to 15kW for Series 1780.";
                    break;
                //Increase power to 20kW for Series 1780
                case 'Rhm5L':
                    HACKS.s1780.powerMaxA = 20000;
                    HACKS.s1780.powerMaxB = 20000;
                    pass = "Changed max power to 20kW for Series 1780.";
                    break;
                 //Increase power to 30kW for Series 1780
                case 'S8aMk':
                    HACKS.s1780.powerMaxA = 30000;
                    HACKS.s1780.powerMaxB = 30000;
                    pass = "Changed max power to 30kW for Series 1780.";
                    break;
                    
                //**** VIBRATION ****
                //Increase vibration to 10 for Series 15xx
                case 'rndak':
                    HACKS.s15xx.vibrationMax = 10;
                    pass = getMessage("changedVibrationLimitforSeries15xx",[10]);
                    break;
                    
                //Increase vibration to 1000 (unlimited) for Series 15xx
                case 'tX8fB':
                    HACKS.s15xx.vibrationMax = 1000;
                    pass = getMessage("changedVibrationLimitforSeries15xx",[1000]);
                    break;
                
                //**** TEMPERATURE ****
                //Increase temperature limit to 180C (for custom solution based on PT100)
                case '4ZWxS':
                    HACKS.s15xx.temperatureMax = 180;
                    pass = "Temperature limit lifted to 180C";
                    warrantyVoid = false;
                    break;
                    
                default:
                    fail = getMessage("errorUnkownHackCode");
            }
            if(warrantyVoid && fail===""){
              fail=getMessage("warrantyVoid") + " ";
            }
            $('.tab-debug #redHack').html(fail);
            $('.tab-debug #greenHack').html(pass);
            if($(this).val() !== 'clear')
              saveHacks();
        } );
	
        // load changelog content
		GUI.refresh_log();
        $('div.changelog.configurator .wrapper').load('./changelog.html');

        if (callback) callback();
    });
};

TABS.debug.cleanup = function (callback) {
    if (callback) callback();
};